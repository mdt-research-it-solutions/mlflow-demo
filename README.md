# MLflow WUR service

- [MLflow WUR service](#mlflow-wur-service)
  - [Check access to project space](#check-access-to-project-space)
  - [User / Role management](#user--role-management)
  - [Install demo requirements](#install-demo-requirements)
  - [Set environment variables](#set-environment-variables)
  - [Run demo and check results](#run-demo-and-check-results)
  - [Setup S3 interface](#setup-s3-interface)
    - [S3 interface application \[Windows/mac only\]](#s3-interface-application-windowsmac-only)
  - [Troubleshoot](#troubleshoot)


## Check access to project space

At this point every member of your project team should have a email with the following credentials:
- Web URL to your project space
- S3 credentials
- User credentials

Everyone should be following the web URL. If you see a MLflow project space after filling in your user credentials, continue with the next steps, else contact mdtresearchitsolutions@wur.nl .

NOTE: You need to be on campus network (campus internet/wifi or VPN) in order to reach the project space.

## User / Role management

Every initial member of the project group has the admin role. Members of the project group are free to add new members. 

NOTE: The default permission of new users is NO_PERMISSIONS. By making a new user admin, they get all permissions. It is also possible to grant the new user specific permissions to experiments and models. See [documentation](https://mlflow.org/docs/latest/auth/index.html) for more information.

## Install demo requirements

  - pip install -r requirements.txt

## Set environment variables

The MLflow project space check authentication using user credentials. Only members of the project team have access to the project space.

This means that if we want to send data to the MLflow project space using a python script, we also need to give the script some authentication method. This is done by setting evironment variables in your terminal.

**NOTE:** Every time you close the terminal you need to set the environment variable again.
- set MLFLOW_TRACKING_USERNAME=user name
- set MLFLOW_TRACKING_PASSWORD=password
  - (use export ... if in linux)

You also need to set the tracking URI (URL to your project space) as an environment variable. 

**NOTE:** This can also be set in the python script by using mlflow.set_tracking_uri("URI")

- set MLFLOW_TRACKING_URI=Web URL to your project space
  - (use export ... if in linux)

## Run demo and check results

There is a python file: **log_simple_experiment.py** that can be used to test MLflow logging by running scripts from the command line.

-  python log_simple_experiment.py

Check results in your MLflow project space. There should be a logged experiment and model. 

## Setup S3 interface

Artifacts logged to the MLflow space are saved to the S3 storage of your project. In order to move, copy or delete these artifacts it is needed to interface with this storage.

- Follow install instruction at https://min.io/docs/minio/linux/reference/minio-mc.html
  - For linux follow intel 64
- Follow Minio server example. Hostname is in the format: https://hostname:443/
- list s3 bucket content:
  - mc ls ALIAS/bucket_name
- Look at Command quick reference for MC commands

### S3 interface application [Windows/mac only]

You can use any s3 interface, however we will show how to setup CloudBerry as a example:

- Go to https://www.msp360.com/explorer/ for windows or macOS users 
- Download relevant installer 
- Install installer 
- Start CloudBerry explorer
- Click File -> Add New Account

![image](readme_images/cloudberry1.png)

- Select S3 Compatible

![image](readme_images/cloudberry2.png)

- Put in **Display name:** name of your choice
- Put in **Server:** the endpoint with https:// 
- Put in **Access Key ID:** the s3 access key 
- Put in **Secret Access Key:** the s3 secret key  
- Click Test connection 
- Continue on successful test

![image](readme_images/cloudberry3.png)

- Select your s3 account via drop down menu at source
- You can now drag files/folders to s3 storage or move files from one source to s3 via interface.  

![image](readme_images/cloudberry4.png) 

## Troubleshoot

If there are any trouble or if you have any feedback/feature request, please contact us at:

    mdtresearchitsolutions@wur.nl
